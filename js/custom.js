$(function(){
    // make calculator draggable
            $('.wrapper').draggable({
                handle:'.glyphicon'
            });

    // type only number
            $('#result').keypress(function(e){
            if(e.which == 48 || e.which == 49 || e.which == 50 || e.which == 51 || e.which == 52 || e.which == 53 || e.which == 54 || e.which == 55 || e.which == 56 || e.which == 57 || e.which == 46 || e.which == 8){
            } else {
              return false;
            }
          });
    
    // set default display value
            $('#result').val(0);
    
    // event on number clicking
            $('.numpad').click(function(){
                var numpad = $(this).text();
                if($('#result').val() == '0' || $('#result').val() == $('#result').data('atr1') || $('#result').data('equal') == true){
                    $('#result').val(numpad);
                }
                else{
                    $('#result').val($('#result').val() + numpad);
                }
                $('#result').data('equal',false);                                
            });
    
    // event on point clicking
            $('.point').click(function(){
                var point = $(this).text();
                $('#result').val($('#result').val() + point);
                if($('#result').data('atr1') || $('#result').data('equal') == true){
                    $('#result').val('0.')
                }
                $('.point').attr('disabled',true);
                $('#result').data('equal',false);
            });    
    
    // event on math operator clicking
            $('.oper').click(function(){                
                var oper = $(this).text();
                $('#result').data('oper',oper);
                var atr1 = parseFloat($('#result').val());
                $('#result').data('atr1',atr1);
                $('.point').attr('disabled',false);
                
    // make some blick on math operator clicking            
                $('input').css('color','#E3E3E3');
                setTimeout("$('input').css('color','#333');",100);
            });
    
    // change tha value variable '+' or '-'
            $('.change').click(function() {
                if($('#result').val() === '0'){
                    $('#result').val('-');
                }
                else{
                    var curVal = $('#result').val();
                    $('#result').val(curVal*(-1));
                }
            });
    
    // event on equal button clicking    
            $('.equal').click(function(){
                var atr2 = parseFloat($('#result').val());
                $('#result').data('atr2',atr2);
                if($('#result').data('oper') == '+'){
                    $('#result').val($('#result').data('atr1') + atr2);
                }
                else if($('#result').data('oper') == '-'){
                    $('#result').val($('#result').data('atr1') - atr2);
                }
                else if($('#result').data('oper') == 'x'){
                    $('#result').val($('#result').data('atr1') * atr2);
                }
                else if($('#result').data('oper') == '/'){
                    $('#result').val($('#result').data('atr1') / atr2);
                }
                else if($('#result').data('oper') == 'xy'){
                    $('#result').val(Math.pow($('#result').data('atr1'),atr2));
                }
                $('.point').attr('disabled',false);
                $('#result').data('atr1','');
                $('#result').data('atr2','');
                $('#result').data('oper','');
                $('#result').data('equal',true);                
            });
    
    // event on clear button clicking        
            $('.clear').click(function(){
                $('input').val(0);
                $('.point').attr('disabled',false);                
                $('#result').data('atr1','');
                $('#result').data('atr2','');
                $('#result').data('oper','');
            });
    
    // event on backspace button clicking
            $('.backspace').click(function() {
                var stringValue = $('#result').val();
                var reduced = stringValue.substr(0,stringValue.length-1);
                if(stringValue.length > 1){
                    $('#result').val(reduced);
                }
                else{
                    $('#result').val('0');
                }                   
            });

    // events on '+', '-', '*', '/', '^' keypress
            $( "#result" ).keypress(function(e){
                if(e.which == 42 || e.which == 43 || e.which == 45 || e.which == 47 || e.which == 94){
                    var keyVal = e.which;
                    var keyOper;
                    if(keyVal == 42){
                        keyOper = 'x';
                    }
                    else if(keyVal == 43){
                         keyOper = '+';
                    }
                    else if(keyVal == 45){
                        keyOper = '-';
                    }
                    else if(keyVal == 47){
                        keyOper = '/';
                    }
                    else if(keyVal == 94){
                        keyOper = 'xy';
                    }                    
                    $('#result').data('oper',keyOper);
                    var keyAtr1 = parseFloat($('#result').val());
                    $('#result').data('atr1',keyAtr1);
                    $('#result').val('');
                    console.log($('#result').data('atr1'));
               }
            });
    
            // events on '=', 'enter' keypress
            $( "#result" ).keypress(function(e){
                if(e.which == 61 || e.which == 13){
                    var keyAtr2 = parseFloat($('#result').val());
                    $('#result').data('atr2',keyAtr2);
                    if($('#result').data('oper') == '+'){
                    $('#result').val($('#result').data('atr1') + keyAtr2);
                    }
                    else if($('#result').data('oper') == '-'){
                        $('#result').val($('#result').data('atr1') - keyAtr2);
                    }
                    else if($('#result').data('oper') == 'x'){
                        $('#result').val($('#result').data('atr1') * keyAtr2);
                    }
                    else if($('#result').data('oper') == '/'){
                        $('#result').val($('#result').data('atr1') / keyAtr2);
                    }
                    else if($('#result').data('oper') == 'xy'){
                        $('#result').val(Math.pow($('#result').data('atr1'),keyAtr2));
                    }
                }
                });
    });